from django.shortcuts import render,redirect
import csv, io
import json
from .models import Teachers_Details
from django.contrib.auth.models import User, auth
from django.contrib import messages 

# Create your views here.

def index(request):
	return render(request, 'directory_app/index.html') 

def home(request):
	return render(request, 'directory_app/home.html')


def upload(request):
	if request.method == "POST":
		try:
			file_object = request.FILES["file"]
			data_set = file_object.read().decode('UTF-8')
			io_string = io.StringIO(data_set)
			next(io_string)
			for column in csv.reader(io_string):
				# print(column)
				first_name = column[0]
				last_name = column[1]
				profile_picture = column[2]
				email_address = column[3]
				phone_number = column[4]
				room_number = column[5]
				subjects_taught = column[6].split(",")
				if len(subjects_taught) > 5:
					subjects_taught = subjects_taught[:5]
				subjects_taught = ",".join(subjects_taught)
				if Teachers_Details.objects.filter(email_address=email_address).exists():
					pass
				else:
					Teachers_data = Teachers_Details(first_name= first_name, last_name= last_name, 
							profile_picture= profile_picture, email_address= email_address, 
							phone_number= phone_number, room_number= room_number,
							subjects_taught = subjects_taught)
					Teachers_data.save()

			return render(request, 'directory_app/upload.html')
		except Exception as e:
			print(e)
			return render(request, 'directory_app/error.html')

	else:
		return render(request, 'directory_app/home.html')


def  details(request):
	try:
		filtered_data = None
		if request.method == "POST":
			radio_check = request.POST['search_radio']
			if radio_check:				
				search_value = request.POST['search_value']
				if search_value:
					if radio_check == "search_name":
						filtered_data = Teachers_Details.objects.filter(last_name__startswith= search_value)
					else:
						filtered_data = Teachers_Details.objects.filter(subjects_taught__contains= search_value)
				return render(request, "directory_app/details.html", {'teachers_details': filtered_data})
		
		else:	
			teachers_details = Teachers_Details.objects.all()
			return render(request, "directory_app/details.html", {'teachers_details': teachers_details})
		
	except Exception as e:
		print(e)
		return render(request, 'directory_app/error.html')


def login(request):
	try:
		if request.method == "POST":
			username = request.POST['user_name']
			password = request.POST['password1']
			if username and password:
				user = auth.authenticate(username= username, password= password)
				if user is not None:
					auth.login(request, user)
					return redirect("home")
				else:
					messages.info(request, "Invalid Credentials")
					return redirect('login')
			else:
				messages.info(request, "Invalid Credentials")
				return redirect('login')
		else:
			return render(request, "directory_app/login.html", {"flag": True})

		
	except Exception as e:
		print(e)
		return render(request, 'directory_app/error.html')


def logout_view(request):
	auth.logout(request)
	return render(request, 'directory_app/index.html')
