from django.db import models
from django.views.generic import ListView

# Create your models here.

class Teachers_Details(models.Model):
	first_name = models.CharField(max_length= 200)
	last_name = models.CharField(max_length= 100)
	profile_picture = models.ImageField(upload_to= 'pics')
	email_address = models.EmailField(max_length= 200)
	phone_number = models.IntegerField()
	room_number = models.CharField(max_length= 200)
	subjects_taught = models.CharField(max_length= 500) 
