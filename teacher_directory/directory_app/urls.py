from django.urls import path
from . import views
  

urlpatterns=[
	path("", views.index, name="index"),
	path("upload", views.upload,name="upload"),
	path("details", views.details, name="details"),
	path("login", views.login, name="login"),
	path("home", views.home, name="home"),
	path("logout", views.logout_view, name="logout_view")
]